package week1;

public class Profesor {
	protected long id;
	protected String Nume;
	protected String Prenume;
	protected int varsta;
	public Profesor()
	{
		id=0;
		Nume="";
		Prenume="";
		varsta=0;
	}
	public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        Profesor prof = (Profesor) obj;
        return this.varsta == prof.varsta && (this.id == prof.id || (this.Nume.equals(prof.Nume))) && this.Prenume.equals(prof.Prenume);
    }
	public int hashcode()
	{
		int hash=5;			//arbitrar
		hash=31*hash+(int)(id ^ (id >>> 32));		
		return hash;
	}
}
