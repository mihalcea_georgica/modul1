package week1;

public class Materie {
	protected long id;
	protected String denumire;
	protected Profesor profesor;
	public Materie()
	{
		id=0;
		denumire="";
		profesor=new Profesor();
	}
	public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        Materie mat = (Materie) obj;
        return this.id == mat.id && (this.denumire == mat.denumire || (this.denumire != null && this.denumire.equals(mat.denumire))) && this.profesor.equals(mat.profesor);
    }
	public int hashcode()
	{
		int hash=7;
		hash=31*hash+(int)(id ^ (id >>> 32));
		return hash;
	}

}
