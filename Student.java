package week1;

public class Student {
	protected long id;
	protected String Nume;
	protected String Prenume;
	protected int varsta;
	protected int anStudiu;
	protected String Facultate;
	public Student()
	{
		Nume="";
		Prenume="";
		varsta=0;
		anStudiu=0;
		Facultate="";
	}
	public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }
        Student stud = (Student) obj;
        return this.varsta == stud.varsta && (this.anStudiu == stud.anStudiu || (this.Nume.equals(stud.Nume)) && this.Facultate.equals(stud.Facultate)) && this.Prenume.equals(stud.Prenume);
    }
	public int hashcode()
	{
		int hash=6;
		hash=31*hash+(int)(id ^ (id >>> 32));
		return hash;
	}

}
